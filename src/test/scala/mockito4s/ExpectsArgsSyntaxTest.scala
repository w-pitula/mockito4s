package mockito4s

import mockito4s.testutils.SimpleMockitoSupport
import utest._

object ExpectsArgsSyntaxTest extends TestSuite {

  val tests = Tests {
    "ExpectsArgsSyntax" - {

      "should handle multi argument methods" - {
        new SimpleMockitoSupport {
          val f = mock[Foo1]

          f.expectsArgs(1, 2L).to(_.foo2(_,_)).thenReturn("r1")
          f.expectsArgs(1).to(_.foo1(_)).thenReturn("r1")

        }
      }

    }
  }

  trait Foo1 {
    def foo0(): String
    def foo1(arg: Int): String
    def foo2(arg1: Int, arg2: Long): String
  }

}
