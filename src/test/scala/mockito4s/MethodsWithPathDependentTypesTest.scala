package mockito4s

import mockito4s.testutils.{SimpleMockitoSupport, SimpleScalaMockMockFactory}
import utest._

object MethodsWithPathDependentTypesTest extends TestSuite {
  override def tests = Tests {
    "scalamock cannot mock types with methods using type parametrized methods" - {
      // we cannot test this because compilation error coming from scalamock is thrown during macro expansion
      // and `compileError` from utest is also a macro. But it still can be checked by manually uncommenting the line
      new SimpleScalaMockMockFactory {
        //          mock[Repo]
      }
    }
    "mockito handles it correctly" - {

      new SimpleMockitoSupport {

        val m = mock[Repo]

      }

    }
  }


  trait SlickApi {
    type Query
  }

  trait Repo {

    val api: SlickApi

    def query: api.Query

  }

}
