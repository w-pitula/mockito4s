package mockito4s.testutils

class SimpleScalaMockMockFactory extends org.scalamock.MockFactoryBase {
  override type ExpectationException = RuntimeException
  override protected def newExpectationException(
    message: String,
    methodName: Option[Symbol]): RuntimeException =
    new RuntimeException(s"$message; $methodName")
}

