package mockito4s.testutils

import mockito4s.MockitoSupport

import scala.collection.mutable.ListBuffer

class SimpleMockitoSupport extends MockitoSupport {

  var verifcations: ListBuffer[() => Unit] = ListBuffer()

  override def addVerification(verify: => Unit): Unit = verifcations.+=(() => verify)

  def verify() = verifcations.foreach(_.apply())
}
