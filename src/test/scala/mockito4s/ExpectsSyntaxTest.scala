package mockito4s

import mockito4s.testutils.SimpleMockitoSupport
import utest._

object ExpectsSyntaxTest extends TestSuite {

  val tests = Tests {
    "ExpectsSyntax" - {

      "should handle zero arg methods" - {
        new SimpleMockitoSupport {
          val f = mock[Foo1]

          f.expects0(_.foo0()).thenReturn("r1")
          f.expects0(_.foo0).thenReturn("r1")
          f.expects0(_.foo0_noParnes).thenReturn("r1")
        }
      }

      "should handle multi argument methods" - {
        new SimpleMockitoSupport {
          val f = mock[Foo1]

          f.expects1(_.foo1)(1).thenReturn("r1")
          f.expects2(_.foo2)(1, 2L).thenReturn("r2")

        }
      }

      // it should be technically possible to support multiple arguments lists with this syntax as well
      // but it would require even more clunky method names. Feel free to PR.

    }
  }

  trait Foo1 {
    def foo0(): String
    def foo0_noParnes: String
    def foo1(arg: Int): String
    def foo2(arg1: Int, arg2: Long): String
  }

}
