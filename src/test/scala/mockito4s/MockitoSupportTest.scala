package mockito4s

import mockito4s.testutils.SimpleMockitoSupport
import org.mockito.exceptions.base.MockitoAssertionError
import shapeless._
import shapeless.ops.function.FnFromProduct
import utest._

object MockitoSupportTest extends TestSuite {

  val tests = Tests {
    "MockitoSupport" - {

      "should support verification without duplicating method calls" - {
        new SimpleMockitoSupport {
          val f = mock[Foo1]

          f.expects1(_.foo)(1).thenReturn("r1")
          f.expects1(_.foo)(2).thenReturn("r2")

          val x = f.foo(1)

          assert(x == "r1")
          intercept[MockitoAssertionError] {
            verify()
          }

          f.foo(2)
          verify()
        }
      }

      "should support overloaded methods" - {
        new SimpleMockitoSupport {
          val f = mock[Foo2]

          f.expectsArgs(1).to(_.foo(_)).thenReturn("r1")
          f.expectsArgs(Seq(1, 2)).to(_.foo(_)).thenReturn("r2")

          val x1 = f.foo(1)
          val x2 = f.foo(Seq(1, 2))

          assert(x1 == "r1")
          assert(x2 == "r2")
        }
      }

      "should support methods with implicit args without type annotations" - {
        new SimpleMockitoSupport {
          val f = mock[Foo3]

          f.expectsArgs(1, 2L).to(_.foo(_)(_)).thenReturn("res")

          implicit val ev: Long = 2L
          val x = f.foo(1)

          assert(x == "res")
        }
      }

      "should exposed aliased Mockito instance" - {
        //not so easy, would have to reimplement mockito.Mockito
      }



    }
  }

  trait Foo1 {
    def foo(a: Int): String
  }

  trait Foo2 {
    def foo(a: Int): String
    def foo(a: Seq[Int]): String
  }

  trait Foo3 {
    def foo(a: Int)(implicit ev: Long): String
    def foo3(implicit ev: Long): String
  }

  trait Foo4[T]{
    def foo(a: T): String
  }
}