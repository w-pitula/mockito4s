package mockito4s

import org.mockito.{Matchers, Mockito}

trait Matchers {

  def *[T] = Matchers.any[T]()

  def where[T](verify: T => Boolean)

}
