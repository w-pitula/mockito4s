package mockito4s

import org.mockito.stubbing.OngoingStubbing
import org.mockito.{MockSettings, Mockito}

import scala.reflect.ClassTag

trait MockitoSupportBase {

  trait MockMarker

  def mock[T](mockSettings: MockSettings)(implicit ct: ClassTag[T]): T with MockMarker =
    Mockito.mock(ct.runtimeClass, mockSettings).asInstanceOf[T with MockMarker]
  def mock[T](implicit ct: ClassTag[T]): T with MockMarker =
    Mockito.mock(ct.runtimeClass).asInstanceOf[T with MockMarker]


  def mockAndVerify[R](expectation: Expectation[R]): OngoingStubbing[R] = {
    addVerification(expectation.verify())
    expectation.mock()
  }

  def addVerification(verify: => Unit)


}
