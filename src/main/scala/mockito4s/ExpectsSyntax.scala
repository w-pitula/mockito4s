package mockito4s

import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing

trait ExpectsSyntax { self: MockitoSupportBase =>

  trait MockedFunc[F] {
    type Mock
    type Args
    type Result

    def apply(f: F): Mock => Args => Result
  }


  implicit class ExpectsOps[T](mock: T with MockMarker) {
    def expects0[U, R](f: T => R): OngoingStubbing[R] = ExpectsNoArg(mock, f)()

    def expects1[U, R](f: T => U => R): Expects[T, U, R] = Expects(mock, f)

    def expects2[A1, A2, R](f: T => (A1, A2) => R): Expects[T, (A1, A2), R] = Expects(mock, m => a => f(m)(a._1, a._2))
  }

  case class Expects[Mock, Args, Results](mock: Mock,
    f: Mock => Args => Results) {

    def apply(args: Args): OngoingStubbing[Results] = mockAndVerify(ExpectationImpl(mock, args, f))

  }

  case class ExpectsNoArg[Mock, Results](mock: Mock,
    f: Mock => Results) {

    def apply(): OngoingStubbing[Results] = {
      val ff: Mock => Unit => Results = m => u => f(m)
      mockAndVerify(ExpectationImpl(mock, (), ff))
    }

  }

}
