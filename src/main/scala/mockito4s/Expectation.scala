package mockito4s

import org.mockito.Mockito
import org.mockito.stubbing.OngoingStubbing


trait Expectation[Result] {
  def mock(): OngoingStubbing[Result]

  def verify(): Result
}


case class ExpectationImpl[Mock, Args, Result](mockObj: Mock, args: Args, fun: Mock => Args => Result) extends Expectation[Result] {
  def mock(): OngoingStubbing[Result] ={
    Mockito.when(fun(mockObj)(args))
  }

  def verify(): Result = {
    fun(Mockito.verify(mockObj))(args)
  }
}
