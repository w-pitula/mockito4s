package mockito4s

import org.mockito.stubbing.OngoingStubbing

trait ExpectsArgsSyntax {
  self: MockitoSupportBase =>

  implicit class ExpectsMultiArgsOps[T](mock: T with MockMarker) {
    def expectsArgs[Arg1](arg1: Arg1) = ExpectsMultiArgs1(mock, arg1)

    def expectsArgs[Arg1, Arg2](args: (Arg1, Arg2)) =
      ExpectsMultiArgs2(mock, args)

    def expectsArgs[Arg1, Arg2, Arg3](arg1: Arg1, arg2: Arg2, arg3: Arg3) =
      ExpectsMultiArgs3(mock, (arg1, arg2, arg3))

    def expectsArgs[Arg1, Arg2, Arg3, Arg4](arg1: Arg1,
      arg2: Arg2,
      arg3: Arg3,
      arg4: Arg4) =
      ExpectsMultiArgs4(mock, (arg1, arg2, arg3, arg4))
  }

  case class ExpectsMultiArgs1[Mock, Arg1](mock: Mock, args: Arg1) {
    def to[Results](
      f: (Mock, Arg1) => Results
    ): OngoingStubbing[Results] = mockAndVerify(ExpectationImpl(mock, args, FuncWrap2.wrap(f)))
  }

  case class ExpectsMultiArgs2[Mock, Arg1, Arg2](mock: Mock, args: (Arg1, Arg2)) {
    def to[Results](
      f: (Mock, Arg1, Arg2) => Results
    ): OngoingStubbing[Results] = mockAndVerify(ExpectationImpl(mock, args, FuncWrap2.wrap(f)))
  }

  case class ExpectsMultiArgs3[Mock, Arg1, Arg2, Arg3](mock: Mock, args: (Arg1, Arg2, Arg3)) {
    def to[Results](
      f: (Mock, Arg1, Arg2, Arg3) => Results
    ): OngoingStubbing[Results] = mockAndVerify(ExpectationImpl(mock, args, FuncWrap2.wrap(f)))
  }

  case class ExpectsMultiArgs4[Mock, Arg1, Arg2, Arg3, Arg4](mock: Mock,
    args: (Arg1, Arg2, Arg3, Arg4)) {
    def to[Results](
      f: (Mock, Arg1, Arg2, Arg3, Arg4) => Results
    ): OngoingStubbing[Results] = mockAndVerify(ExpectationImpl(mock, args, FuncWrap2.wrap(f)))
  }

  object FuncWrap2 {

    def wrap[A1, R](f: A1 => R): A1 => Unit => R = a1 => r => f(a1)

    def wrap[A1, A2, R](f: (A1, A2) => R): A1 => A2 => R = a1 => r => f(a1, r)

    def wrap[A1, A2, A3, R](f: (A1, A2, A3) => R): A1 => ((A2, A3)) => R = a1 => r => f(a1, r._1, r._2)

    def wrap[A1, A2, A3, A4, R](f: (A1, A2, A3, A4) => R): A1 => ((A2, A3, A4)) => R = a1 => r => f(a1, r._1, r._2, r._3)

    def wrap[A1, A2, A3, A4, A5, R](f: (A1, A2, A3, A4, A5) => R): A1 => ((A2, A3, A4, A5)) => R = a1 => r => f(a1, r._1, r._2, r._3, r._4)

  }

}
