organization := "com.sony.crow"

name := "mockito4s"

version := "0.1-SNAPSHOT"

scalaVersion := "2.12.6"

libraryDependencies ++= Seq(
  "org.mockito" % "mockito-all" % "1.10.19",
  "com.chuusai" %% "shapeless" % "2.3.3",
  "com.lihaoyi" %% "utest" % "0.6.3" % "test",
  "org.scalamock" %% "scalamock" % "4.1.0" % Test,
  "com.softwaremill.sttp" %% "core" % "1.2.3"

)

testFrameworks += new TestFramework("utest.runner.Framework")


//addCompilerPlugin("org.spire-math" %% "kind-projector" % "0.9.7")

// where clause strange errors