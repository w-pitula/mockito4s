# mockito4s

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Why?](#why)
- [Features](#features)
  - [Implicit verification](#implicit-verification)
  - [Boilerplate-free mocks of overloaded methods](#boilerplate-free-mocks-of-overloaded-methods)
  - [Boilerplate-free mocks of implicit parameters](#boilerplate-free-mocks-of-methods-using-implicit-parameters)
  - [Proper handling of mocks using path-dependent types](#proper-handling-of-mocks-using-path-dependent-types)
  - [Arguments matchers](#arguments-matchers)
  - [Mixed arguments matchers](#mixed-arguments-matchers)
  - [More typesafety](#more-typesafety)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Why?
mockito4s tries to bring more convenient api for mockito and fix some its issued when being used from scala. 
It is heavily inspired by ScalaMock and tries to bring similar capabilities but without it limitations.


## Features 

### Implicit verification
TODO document

### Boilerplate-free mocks of overloaded methods
TODO document

### Boilerplate-free mocks of methods using implicit parameters
TODO document

### Proper handling of mocks using path-dependent types
TODO document

### Arguments matchers
TODO implement

### Mixed arguments matchers
TODO implement

### More typesafety 
TODO document

